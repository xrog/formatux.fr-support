////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Antoine Le Morvan
////

= Ordonnanceur centralisé Rundeck
ifndef::doctype[]
:doctype: article
:encoding: utf-8
:lang: fr
:numbered:
:sectnumlevels: 3
:description: Ordonnanceur centralisé Rundeck
:revnumber: 1.0
:revdate: 23-08-2017
:experimental:
:source-highlighter: highlightjs
:pdf-page-size: A4
:source-language: bash
endif::[]

.icon:mortar-board[] Objectifs
****
icon:check-square-o[] installer un serveur d'ordonnancement Rundeck ; +
icon:check-square-o[] créer un premier projet et une tâche simple.
****

indexterm2:[Rundeck] est un ordonnanceur centralisé open source (licence Apache) écrit en Java.

Depuis l'interface de Rundeck, il est possible de gérer les différents travaux (commandes ou scripts) à exécuter sur les serveurs distants.

Fonctionnalités :

* Ordonnancement des tâches selon un scénario ;
* Exécution de scripts/tâches distantes à la demande ou de manière planifiée ;
* Notifications ;
* Interface CLI (ligne de commande) et API.

Rundeck peut être intégré aux autres outils de gestion de configuration comme Puppet, Ansible et d'intégration continue comme Jenkins, etc.

La connexion avec les hôtes clients est gérée en SSH.

== Installation 

[NOTE]
====
Rundeck utilisant le protocle SSH pour se connecter aux systèmes distants, un compte avec les droits sudo est nécessaire sur chacun des stations clientes.
====

* sur Debian 8 (Jessie) :

Rundeck étant écrit en Java, l'installation du JDK est nécessaire :

[source,]
----
# apt-get install openjdk-7-jdk
----

Rundeck peut être téléchargé puis installé :

[source,]
----
# wget http://dl.bintray.com/rundeck/rundeck-deb/rundeck-2.6.7-1-GA.deb
# dpkg -i ./rundeck-2.6.7-1-GA.deb
----

* Sur CentOS 7 :

Rundeck étant écrit en Java, l'installation du JDK est nécessaire :

[source,]
----
# yum install java-1.8.0
----

Rundeck peut être téléchargé puis installé :

[source,]
----
# rpm -Uvh http://repo.rundeck.org/latest.rpm
# yum install rundeck
----

== Configuration

Par défaut, Rundeck n'est configuré en écoute que sur l'adresse de boucle interne (`localhost`). 

Si vous ne souhaitez pas mettre en place un proxy inverse (Apache, Nginx ou HAProxy), éditez les fichiers `/etc/rundeck/framework.properties` et `/etc/rundeck/rundeck-config.properties` et remplacer `localhost` par l'adresse IP du serveur, pour mettre en écoute Rundeck sur son interface réseau :

.Modification du fichier /etc/rundeck/framework.properties
[source,]
----
framework.server.url = http://xxx.xxx.xxx.xxx:4440
----

.Modification du fichier /etc/rundeck/rundeck-config.properties
[source,]
----
grails.serverURL=http://xxx.xxx.xxx.xxx:4440
----

[NOTE]
====
Remplacer `xxx.xxx.xxx.xxx` par l'adresse IP publique du serveur.
====

Rundeck est ensuite lancé par systemctl :

[source,]
----
# systemctl start rundeckd
# systemctl enable rundeckd
----

L'interface d'administration de Rundeck est accessible depuis un navigateur internet à l'adresse `http://your_server:4440`.

== Utiliser RunDeck

Le compte par défaut pour se connecter à l'interface web est `admin` (mot de passe : `admin`). Pensez à changer le mot de passe le plus rapidement possible.

.La page d'accueil de RunDeck
image::images/DEVOPS-030-001-welcome.png[]
=== Créer un projet

Un nouveau projet peut être ajouté en cliquant sur le lien `Nouveau projet`.

.L'assistant de création de nouveau projet Rundeck
image::images/DEVOPS-030-002-create-projet.png[]

[NOTE]
====
Vous devez fournir au minimum un nom de projet.
====

Dans la section `Resource Model Source`, cliquer sur le bouton `Edit` et choisir `Require File Exists `, puis cliquer sur `Save`.

Dans la section `Default Node Executor`, choisir `password` pour l'authentification par SSH (pour une meilleure gestion de la sécurité, l'utilisation d'une clef SSH est recommandée).

Cliquer sur `Create` pour créer le projet.

=== Créer une tâche

Le serveur est prêt à recevoir une première tâche. Cette tâche consiste en une connection SSH pour lancer une commande distante.

* Cliquer sur `Create a new job` et saisir un nom pour la tâche.

.L'assistant de création de tâche de RunDeck
image::images/DEVOPS-030-003-new-job.png[]

Il va falloir fournir à RunDeck le mot de passe de l'utilisateur distant permettant de se connecter au serveur ainsi que le mot de passe sudo pour lancer la commande. Pour cela :

* Ajouter une option

Cliquer sur `Add New Option`.

image::images/DEVOPS-030-004-new-option.png[]

Donner un nom pour l'option (par exemple sshPassword) et une valeur par défaut qui correspond à votre mot de passe.

Dans le champ `Input Type`, choisir `Secure Remote Authentication` et mettre `Required` à `Yes`.

Répéter l'opération avec l'option `sudoPassword`.

Dans la section `Add a Step`, choisir `Command`. Fournir la commande dans le champ `Command`. Par exemple :

[source,]
----
sudo "top -b -n 1 | head -n 5"
----

Cliquer sur `Save` puis `Create` pour finaliser la nouvelle tâche.

Pour appliquer cette tâche à un système distant (appelé noeud), éditer le fichier de configuration du noeud :

[source,]
----
vi /var/rundeck/projects/your_project_name/etc/resources.xml
----

Ajouter à la ligne commençant par `localhost` : `ssh-authentication="password" ssh-password-option="option.sshPassword" sudo-command-enabled="true" sudo-password-option="option.sudoPassword"`, pour activer l'authentification par SSH et fournir les valeurs des options pour l'authentification.

Retourner dans l'interface web est executer la tâche.

Le serveur RunDeck est prêt à recevoir vos projets d'ordonnancement.